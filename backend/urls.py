"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

import backend.views
from postcodes import views
from backend.postcodes.views import getzipview


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', backend.views.index)
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))


urlpatterns += [

    url(r'^import_city/', views.import_city, name="import_city"),
    url(r'^import_street/', views.import_street, name="import_street"),
    url(r'^getzip/', getzipview, name="getzipview"),
    # url(r'^export/(.*)', views.export_data, name="export"),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]


"""
    ____            __
   / __ \___  _____/ /_
  / /_/ / _ \/ ___/ __/
 / _, _/  __(__  ) /_
/_/ |_|\___/____/\__/

"""

from django.conf.urls import url, include
from django.contrib.auth.models import User
from backend.postcodes.models import City, Street
from rest_framework import routers, serializers, viewsets, generics

import rest_framework_filters as filters




# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


# Serializers define the API representation.
class StreetSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Street
        fields = ('name', 'city', )




class StreetsReadView(generics.ListAPIView):
    serializer_class = StreetSerializer
    model = serializer_class.Meta.model
    paginate_by = 100
    def get_queryset(self):
        city = self.kwargs['city']
        queryset = self.model.objects.filter(city=city)
        return queryset.order_by('id')





# Serializers define the API representation.
class CitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = City
        fields = ('name', 'city_id', )



# ViewSets define the view behavior.
class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    filter_backends = (filters.backends.DjangoFilterBackend,)



# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'cities', CityViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns += [
    url(r'^api/', include(router.urls)),
    url(r'^api/streets/(?P<city>.+)/$', StreetsReadView.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
