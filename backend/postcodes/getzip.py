# coding:utf-8
import requests
import re
from bs4 import BeautifulSoup
import urllib


http_proxy = "http://192.117.146.110:80"
https_proxy = "https://192.117.146.110:80"

proxyDict = {
    "http": http_proxy,
    "https": https_proxy,
}


# http://israelpost.co.il


def getzip(request):

    city = request.GET.get('city')
    if city:
        city = city.encode('utf-8').strip()
    street = request.GET.get('street')
    if street:
        street = street.encode('utf-8').strip()
    entrance = request.GET.get('entrance')
    if entrance:
        entrance = entrance.encode('utf-8').strip()

    url = 'http://www.israelpost.co.il/zip_data.nsf/SearchZip?OpenAgent&Location={city}&POB={POB}&Street={street}&House={house}'.format(
        city=city, #u'אבירים'.encode('utf-8'),
        POB=u''.encode('utf-8'),
        street = street, #u''.encode('utf-8'),
        house = request.GET.get('house'), # u'1',

    )
    # url = 'https://israelpost.co.il'

    # print "server api URL:", url
    headers = {}
    r = requests.get(
        url,
        headers=headers,
        proxies=proxyDict
    )
    # print(r.text)

    # regm = re.match(r'RES[0-9]*\d', r.text)
    try:
        m = re.search('RES[0-9]*\d', r.text).group()
        # print "MMMMMM", m
        if m == "RES2":
            trimmedZipString = "12"
            soup = BeautifulSoup(r.text)
            error = soup.get_text()
        elif (m == "RES013"):
            trimmedZipString = "13"
            soup = BeautifulSoup(r.text)
            error = soup.get_text()
        elif (m == "RES011"):
            trimmedZipString = "11"
            soup = BeautifulSoup(r.text)
            error = soup.get_text()
        else:
            error = False
            trimmedZipString = m[4:]

        # print ('REQUEST done', r, dir(r))
        return {
            'zip_error': error,
            'trimmedZipString': trimmedZipString
        }

    except Exception:
        return {
            'zip_error': 'Cannot find zip',
            'trimmedZipString': ''
        }



