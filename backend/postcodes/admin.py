# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from backend.postcodes.models import City, Street


# Register your models here.
admin.site.register(City)
admin.site.register(Street)
