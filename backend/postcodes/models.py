# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class City(models.Model):
    name = models.CharField(max_length=200)
    city_id = models.IntegerField(default=0)
    # pub_date = models.DateTimeField('date published')
    # slug = models.CharField(max_length=10, unique=True,
    #                         default="question")

    # def __str__(self):
    #     return self.question_text


class Street(models.Model):
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    city_id = models.CharField(max_length=200)
    street_id = models.CharField(max_length=200)


class Zipcode(models.Model):
    full_address = models.CharField(max_length=500)
    zip_result = models.IntegerField(default=0)



    # votes = models.IntegerField(default=0)

    # def __str__(self):
    #     return self.choice_text
