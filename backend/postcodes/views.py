# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.cache import cache
from django.shortcuts import render
from django.http import HttpResponseBadRequest, HttpResponse

try:
    from django.http import JsonResponse
except ImportError:
    from django.http import HttpResponse
    import json

    def JsonResponse(data):
        return HttpResponse(json.dumps(data),
                            content_type="application/json")


from django import forms
import django_excel as excel
from backend.postcodes.models import City, Street , Zipcode

from backend.postcodes.getzip import getzip


data = [
    [1, 2, 3],
    [4, 5, 6]
]


class UploadFileForm(forms.Form):
    file = forms.FileField()


# Create your views here.
def upload(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            filehandle = request.FILES['file']
            return excel.make_response(
                filehandle.get_sheet(),
                "csv",
                file_name="download")
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {
            'form': form,
            'title': 'Excel file upload and download example',
            'header': ('Please choose any excel file ' +
                       'from your cloned repository:')
        })


def download(request, file_type):
    sheet = excel.pe.Sheet(data)
    return excel.make_response(sheet, file_type)


def download_as_attachment(request, file_type, file_name):
    return excel.make_response_from_array(
        data, file_type, file_name=file_name)


def export_data(request, atype):
    if atype == "sheet":
        return excel.make_response_from_a_table(
            Question, 'xls', file_name="sheet")
    elif atype == "book":
        return excel.make_response_from_tables(
            [Question, Choice], 'xls', file_name="book")
    elif atype == "custom":
        question = Question.objects.get(slug='ide')
        query_sets = Choice.objects.filter(question=question)
        column_names = ['choice_text', 'id', 'votes']
        return excel.make_response_from_query_sets(
            query_sets,
            column_names,
            'xls',
            file_name="custom"
        )
    else:
        return HttpResponseBadRequest(
            "Bad request. please put one of these " +
            "in your url suffix: sheet, book or custom")


def import_data(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)

        def choice_func(row):
            q = Question.objects.filter(
                slug=row[0]
            )[0]
            row[0] = q
            return row
        if form.is_valid():
            request.FILES['file'].save_book_to_database(
                models=[City, Street],
                initializers=[None, None],
                mapdicts=[
                    ['name'],
                    ['name', 'city']
                ]
            )
            return HttpResponse("OK", status=200)
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {
            'form': form,
            'title': 'Import excel data into database example',
            'header': 'Please upload sample-data.xls:'
        })


def import_city(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            request.FILES['file'].save_to_database(
                name_columns_by_row=2,
                model=City,
                mapdict=['city_id', 'name']
            )
            return HttpResponse("OK.Cities imported")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {'form': form})


def import_street(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            request.FILES['file'].save_to_database(
                name_columns_by_row=2,
                model=Street,
                mapdict=['city_id', 'city', 'street_id', 'name']
            )
            return HttpResponse("OK.Streets imported")
        else:
            return HttpResponseBadRequest()
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {'form': form})




def exchange(request, file_type):
    form = UploadFileForm(request.POST, request.FILES)
    if form.is_valid():
        filehandle = request.FILES['file']
        return excel.make_response(filehandle.get_sheet(), file_type)
    else:
        return HttpResponseBadRequest()


def parse(request, data_struct_type):
    form = UploadFileForm(request.POST, request.FILES)
    if form.is_valid():
        filehandle = request.FILES['file']
        if data_struct_type == "array":
            return JsonResponse({"result": filehandle.get_array()})
        elif data_struct_type == "dict":
            return JsonResponse(filehandle.get_dict())
        elif data_struct_type == "records":
            return JsonResponse({"result": filehandle.get_records()})
        elif data_struct_type == "book":
            return JsonResponse(filehandle.get_book().to_dict())
        elif data_struct_type == "book_dict":
            return JsonResponse(filehandle.get_book_dict())
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseBadRequest()


def getzipview(request):

    full_address = "{city} {street} {house}".format(
        city=request.GET.get('city'),
        street=request.GET.get('city'),
        house=request.GET.get('city')
    )
    print ('AAAAAA', full_address)


    # First try get from cache
    cachezip = cache.get(full_address)
    if cachezip:
        return JsonResponse({
            "zip": cachezip,
            "zip_error": False,
            "source": "cache"
        })
    # Second Try to get from Database
    try:
        dbzip = Zipcode.objects.get(full_address=full_address)
        return JsonResponse({
            "zip": dbzip.zip_result,
            "zip_error": False,
            "source": "database"
        })
    except Exception:
        pass  # no zip found from db


    # Third get zip from Israel post API and save it to DB and Cache if no error
    result = getzip(request)
    if result['zip_error'] is False:
        Zipcode.objects.create(
            full_address=full_address,
            zip_result=result['trimmedZipString']
        )
        cache.set(full_address, result['trimmedZipString'])

    # print ("RESSSSs", result)
    if result:
        return JsonResponse({
            "zip": result['trimmedZipString'],
            "zip_error": result['zip_error'],
            "source": "api"
        })
    else:
        return JsonResponse({"error": "Error"})

