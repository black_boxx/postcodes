# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-24 16:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('postcodes', '0003_auto_20170619_1422'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zipcode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_address', models.CharField(max_length=500)),
                ('zip_result', models.IntegerField(default=0)),
            ],
        ),
    ]
